/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bds;

import com.mycompany.DTO.Editorial;
import com.mycompany.DTO.Libro;
import com.mycompany.DTO.Usuario;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Base64;
import org.json.simple.JSONObject;
//import org.apache.commons.net.util.Base64;
import org.json.simple.parser.JSONParser;
import org.json.simple.JSONArray;

/**
 *
 * @author Compu Fire
 */
public class PerUsuario {
    
    public int actualizarUsuario(Usuario usu){
        int flag = 0;
        Conexion con = new Conexion();
        JSONObject datos = new JSONObject();
        datos.put("tabla", "Usuario");
        datos.put("codigo", "2");
        datos.put("Cod_usuario", usu.getCod_usuario());
        datos.put("Nombre_usuario", usu.getNombre_usuario());
        datos.put("Direccion_usuario", usu.getDireccion_usuario());
        datos.put("Telefono", usu.getTelefono());
        datos.put("Estado", usu.getEstado());
        datos.put("User_usuario", usu.getUser_usuario());
        datos.put("Contrasena", usu.getContrasena());
        datos.put("c1", usu.getCod_puesto());
        
        JSONObject jsonResp = con.crearConexion(datos);
        
        if(jsonResp!=null){
            if(jsonResp.get("respuesta").toString().equalsIgnoreCase("1")){
                flag=1;
            }
        }
        
        return flag;
    }
    
    public List<Usuario> findAllUsuario(){
        Conexion con = new Conexion();
        List<Usuario> lista = new ArrayList<Usuario>();
        JSONObject datos = new JSONObject();
        datos.put("tabla", "Usuario");
        datos.put("codigo", "3");
        
        JSONObject jsonResp = con.crearConexion(datos);
        JSONArray listaJson = new JSONArray();
        for(int i=0; i<jsonResp.size(); i++){
            Usuario l1 = new Usuario();
            JSONObject jsonResp1 = new JSONObject();
            String c= Integer.toString(i);
            jsonResp1 = (JSONObject) jsonResp.get(c);
            l1.setCod_usuario(Integer.parseInt(jsonResp1.get("Cod_usuario").toString()));
            l1.setCod_puesto(Integer.parseInt(jsonResp1.get("Cod_puesto2").toString()));
            l1.setNombre_puesto((String) jsonResp1.get("Cod_puesto"));
            l1.setNombre_usuario((String) jsonResp1.get("Nombre_usuario"));
            l1.setDireccion_usuario((String) jsonResp1.get("Direccion_usuario"));
            l1.setTelefono(Integer.parseInt(jsonResp1.get("Telefono").toString()));
            l1.setEstado(Integer.parseInt(jsonResp1.get("Estado").toString()));
            l1.setUser_usuario((String) jsonResp1.get("User_usuario"));
            l1.setContrasena((String) jsonResp1.get("Contrasena"));
            lista.add(l1);
        }
        
        return lista;
    }
   
    public int actualizarCuentaEspecial(Usuario usu){
        int flag=0;
        Conexion con = new Conexion();
        String contra=usu.getContrasena();
        JSONObject datos = new JSONObject();
        datos.put("tabla", "Usuario");
        datos.put("codigo", "2");
        datos.put("Cod_usuario", usu.getCod_usuario());
        datos.put("Nombre_usuario", usu.getNombre_usuario());
        datos.put("Direccion_usuario", usu.getDireccion_usuario());
        datos.put("Telefono", usu.getTelefono());
        datos.put("Estado", usu.getEstado());
        datos.put("User_usuario", usu.getUser_usuario());
        datos.put("Contrasena", usu.getContrasena());
        datos.put("c1", usu.getCod_puesto());
        
        JSONObject jsonResp = con.crearConexion(datos);
        
        if(jsonResp!=null){
            if(jsonResp.get("respuesta").toString().equalsIgnoreCase("1")){
                flag=1;
            }
        }
        
        return flag;
    }
    
    public List<Usuario> cuenta(int id){
        List<Usuario> lista = new ArrayList<Usuario>();
        Conexion con = new Conexion();
        Usuario usuario = new Usuario();
        JSONObject datos = new JSONObject();
        datos.put("tabla", "Usuario");
        datos.put("codigo", "4");
        datos.put("c1", id);
        
        JSONObject jsonResp = con.crearConexion(datos);
        
        usuario.setCod_usuario(id);
        usuario.setNombre_puesto((String) jsonResp.get("Cod_puesto"));
        usuario.setCod_puesto(Integer.parseInt(jsonResp.get("Cod_puesto2").toString()));
        usuario.setNombre_usuario((String) jsonResp.get("Nombre_usuario"));
        usuario.setDireccion_usuario((String) jsonResp.get("Direccion_usuario"));
        usuario.setTelefono(Integer.parseInt(jsonResp.get("Telefono").toString()));
        usuario.setEstado(Integer.parseInt(jsonResp.get("Estado").toString()));
        usuario.setUser_usuario((String) jsonResp.get("User_usuario"));
        usuario.setContrasena((String) jsonResp.get("Contrasena"));
        lista.add(usuario);
        
        return lista;
    }
    
    public int iniciarsesion(String usuario, String contrasena, String opcion, int id){
        int flag=0;
        Conexion con = new Conexion();
        JSONObject datos = new JSONObject();
        datos.put("tabla", "Usuario");
        datos.put("codigo", "4");
        datos.put("c1", id);
        
        JSONObject jsonResp = con.crearConexion(datos);

        if(jsonResp!=null){    
            if(jsonResp.get("User_usuario").toString().equalsIgnoreCase(usuario)){
                if(jsonResp.get("Contrasena").toString().equalsIgnoreCase(contrasena)){
                    if(jsonResp.get("Cod_puesto").toString().equalsIgnoreCase(opcion)){
                        if(opcion.equalsIgnoreCase("Administrador")){
                            flag=1;
                        }else{
                            flag=2;
                        }
                    }else{
                        flag=2;
                    }
                }else{
                    flag=9;
                }
            }else{
                flag=8;
            }
        }else{
            flag=10;
        }
        
        return flag;
    }
    
    public boolean insertar(Usuario usuario){
        boolean band = false;
        Conexion con = new Conexion();
        JSONObject datos = new JSONObject();
        datos.put("tabla", "Usuario");
        datos.put("codigo", "1");
        datos.put("Nombre_usuario", usuario.getNombre_usuario());
        datos.put("Direccion_usuario", usuario.getDireccion_usuario());
        datos.put("Telefono", usuario.getTelefono());
        datos.put("User_usuario", usuario.getUser_usuario());
        datos.put("Contrasena", usuario.getContrasena());
        datos.put("c1", Integer.toString(usuario.getCod_puesto()));
        
        JSONObject jsonResp = con.crearConexion(datos);
        
        if(jsonResp.get("respuesta").toString().equalsIgnoreCase("1")){
            band=true;
        }
        
        return band;
    }
    
    public int encontrarId(Usuario usuario){
        JSONObject datos = new JSONObject();
        int num=0;
        Conexion con = new Conexion();
        datos.put("tabla", "Usuario");
        datos.put("codigo", "3");
        
        JSONObject jsonResp = con.crearConexion(datos);
        
        JSONArray listaJson = new JSONArray();
        for(int i=0; i<jsonResp.size(); i++){
            JSONObject jsonResp1 = new JSONObject();
            String c= Integer.toString(i);
            jsonResp1 = (JSONObject) jsonResp.get(c);
            if(jsonResp1.get("Nombre_usuario").toString().equalsIgnoreCase(usuario.getNombre_usuario())){
                if(jsonResp1.get("Direccion_usuario").toString().equalsIgnoreCase(usuario.getDireccion_usuario())){
                    if(jsonResp1.get("User_usuario").toString().equalsIgnoreCase(usuario.getUser_usuario())){
                        if(jsonResp1.get("Contrasena").toString().equalsIgnoreCase(usuario.getContrasena())){
                            if(jsonResp1.get("Cod_puesto").toString().equalsIgnoreCase(usuario.getNombre_puesto())){
                                num=Integer.parseInt(jsonResp1.get("Cod_usuario").toString());
                            }
                        }
                    }
                }
            }
        }
        
        return num;
    }
}
