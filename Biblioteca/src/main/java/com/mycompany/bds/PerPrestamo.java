/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bds;

import com.mycompany.DTO.Editorial;
import com.mycompany.DTO.Libro;
import com.mycompany.DTO.Prestamo;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author Compu Fire
 */
public class PerPrestamo {
    
    public int crearPrestamo(Prestamo pres, int id){
        int flag=0;
        Conexion con = new Conexion();
        JSONObject datos = new JSONObject();
        datos.put("tabla", "Prestamo");
        datos.put("codigo", "1");
        datos.put("Fecha_prestamo", pres.getFecha_prestamo());
        datos.put("Fecha_devolucion", pres.getFecha_devolucion());
        datos.put("c1", pres.getCod_libro());
        datos.put("c2", pres.getCod_usuario());
        
        
        JSONObject jsonResp = con.crearConexion(datos);
        
        if(jsonResp!=null){
            if(jsonResp.get("respuesta").toString().equalsIgnoreCase("1")){
                flag=1;
            }
        }
        
        return flag;
    }
    
    public int actualizarPrestamo(Prestamo pres){
        int flag=0;
        
        Conexion con = new Conexion();
        JSONObject datos = new JSONObject();
        datos.put("tabla", "Prestamo");
        datos.put("codigo", "2");
        datos.put("Cod_prestamo", pres.getCod_prestamo());
        datos.put("Fecha_prestamo", pres.getFecha_prestamo());
        datos.put("Fecha_devolucion", pres.getFecha_devolucion());
        datos.put("c1", pres.getCod_libro());
        datos.put("c2", pres.getCod_usuario());
        
        
        JSONObject jsonResp = con.crearConexion(datos);
        
        if(jsonResp!=null){
            if(jsonResp.get("respuesta").toString().equalsIgnoreCase("1")){
                flag=1;
            }
        }
        
        return flag;
    }
    
    public List<Prestamo> findPrestamosInactivos(){
        Conexion con = new Conexion();
        List<Prestamo> lista = new ArrayList<Prestamo>();
        JSONObject datos = new JSONObject();
        datos.put("tabla", "Prestamo");
        datos.put("codigo", "4");
        
        JSONObject jsonResp = con.crearConexion(datos);
        JSONArray listaJson = new JSONArray();
        for(int i=0; i<jsonResp.size(); i++){
            Prestamo l1 = new Prestamo();
            JSONObject jsonResp1 = new JSONObject();
            String c= Integer.toString(i);
            jsonResp1 = (JSONObject) jsonResp.get(c);
            l1.setCod_prestamo(Integer.parseInt(jsonResp1.get("Cod_prestamo").toString()));
            l1.setCod_libro(Integer.parseInt(jsonResp1.get("Cod_libro2").toString()));
            l1.setNombre_libro((String) jsonResp1.get("Cod_libro"));
            l1.setCod_usuario(Integer.parseInt(jsonResp1.get("Cod_usuario2").toString()));
            l1.setNombre_usuario((String) jsonResp1.get("Cod_usuario"));
            l1.setFecha_prestamo((String) jsonResp1.get("Fecha_prestamo"));
            l1.setFecha_devolucion((String) jsonResp1.get("Fecha_devolucion"));
            l1.setEstado(Integer.parseInt(jsonResp1.get("Estado").toString()));
            lista.add(l1);
        }
        
        return lista;
    }
    
    
    public List<Prestamo> findPrestamosActivos(){
        Conexion con = new Conexion();
        List<Prestamo> lista = new ArrayList<Prestamo>();
        JSONObject datos = new JSONObject();
        datos.put("tabla", "Prestamo");
        datos.put("codigo", "3");
        
        JSONObject jsonResp = con.crearConexion(datos);
        JSONArray listaJson = new JSONArray();
        for(int i=0; i<jsonResp.size(); i++){
            Prestamo l1 = new Prestamo();
            JSONObject jsonResp1 = new JSONObject();
            String c= Integer.toString(i);
            jsonResp1 = (JSONObject) jsonResp.get(c);
            l1.setCod_prestamo(Integer.parseInt(jsonResp1.get("Cod_prestamo").toString()));
            l1.setCod_libro(Integer.parseInt(jsonResp1.get("Cod_libro2").toString()));
            l1.setNombre_libro((String) jsonResp1.get("Cod_libro"));
            l1.setCod_usuario(Integer.parseInt(jsonResp1.get("Cod_usuario2").toString()));
            l1.setNombre_usuario((String) jsonResp1.get("Cod_usuario"));
            l1.setFecha_prestamo((String) jsonResp1.get("Fecha_prestamo"));
            l1.setFecha_devolucion((String) jsonResp1.get("Fecha_devolucion"));
            l1.setEstado(Integer.parseInt(jsonResp1.get("Estado").toString()));
            lista.add(l1);
        }
        
        return lista;
    }
    
}
