/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bds;

import com.mycompany.DTO.Libro;
import com.mycompany.DTO.Usuario;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Base64;
import org.json.simple.JSONObject;
//import org.apache.commons.net.util.Base64;
import org.json.simple.parser.JSONParser;
import org.json.simple.JSONArray;

/**
 *
 * @author Compu Fire
 */
public class PerLibro {
    
    public int actualizarLibro(Libro libro){
        int flag=0;
        Conexion con = new Conexion();
        JSONObject datos = new JSONObject();
        datos.put("tabla", "Libro");
        datos.put("codigo", "2");
        datos.put("Cod_libro", libro.getCod_libro());
        datos.put("Cod_existencia", libro.getExistencia());
        datos.put("Titulo_libro", libro.getTitulo_libro());
        datos.put("c1", libro.getCod_editorial2());
        datos.put("c2", libro.getCod_categoria2());
        datos.put("c3", libro.getCod_autor2());
        
        JSONObject jsonResp = con.crearConexion(datos);
        
        if(jsonResp!=null){
            if(jsonResp.get("respuesta").toString().equalsIgnoreCase("1")){
                flag=1;
            }
        }
        
        return flag;
    }
    
    public List<Libro> findAllLibroUsuario(){
        List<Libro> lista = new ArrayList<Libro>();
        Conexion con = new Conexion();
        JSONObject datos = new JSONObject();
        datos.put("tabla", "Libro");
        datos.put("codigo", "3");
        
        JSONObject jsonResp = con.crearConexion(datos);
        
        JSONArray listaJson = new JSONArray();
        for(int i=0; i<jsonResp.size(); i++){
            Libro l1 = new Libro();
            JSONObject jsonResp1 = new JSONObject();
            String c= Integer.toString(i);
            jsonResp1 = (JSONObject) jsonResp.get(c);
            l1.setCod_libro(Integer.parseInt(jsonResp1.get("Cod_libro").toString()));
            l1.setCod_editorial((String) jsonResp1.get("Cod_editorial"));
            l1.setCod_editorial2((String) jsonResp1.get("Cod_editorial2"));
            l1.setCod_categoria((String) jsonResp1.get("Cod_categoria"));
            l1.setCod_categoria2((String) jsonResp1.get("Cod_categoria2"));
            l1.setCod_autor((String) jsonResp1.get("Cod_autor"));
            l1.setCod_autor2((String) jsonResp1.get("Cod_autor2"));
            l1.setTitulo_libro((String) jsonResp1.get("Titulo_libro"));
            l1.setExistencia(Integer.parseInt(jsonResp1.get("Existencia").toString()));
            lista.add(l1);
        }
        
        return lista;
    }
    
    public List<Libro> findAllLibro(){
        List<Libro> lista = new ArrayList<Libro>();
        Conexion con = new Conexion();
        JSONObject datos = new JSONObject();
        datos.put("tabla", "Libro");
        datos.put("codigo", "4");
        
        JSONObject jsonResp = con.crearConexion(datos);
        
        JSONArray listaJson = new JSONArray();
        for(int i=0; i<jsonResp.size(); i++){
            Libro l1 = new Libro();
            JSONObject jsonResp1 = new JSONObject();
            String c= Integer.toString(i);
            jsonResp1 = (JSONObject) jsonResp.get(c);
            l1.setCod_libro(Integer.parseInt(jsonResp1.get("Cod_libro").toString()));
            l1.setCod_editorial((String) jsonResp1.get("Cod_editorial"));
            l1.setCod_editorial2((String) jsonResp1.get("Cod_editorial2"));
            l1.setCod_categoria((String) jsonResp1.get("Cod_categoria"));
            l1.setCod_categoria2((String) jsonResp1.get("Cod_categoria2"));
            l1.setCod_autor((String) jsonResp1.get("Cod_autor"));
            l1.setCod_autor2((String) jsonResp1.get("Cod_autor2"));
            l1.setTitulo_libro((String) jsonResp1.get("Titulo_libro"));
            l1.setExistencia(Integer.parseInt(jsonResp1.get("Existencia").toString()));
            lista.add(l1);
        }
        
        return lista;
    }
    
    public int crearLibro(Libro lib){
        int flag=0;
        Conexion con = new Conexion();
        JSONObject datos = new JSONObject();
        datos.put("tabla", "Libro");
        datos.put("codigo", "1");
        datos.put("Cod_existencia", lib.getExistencia());
        datos.put("Titulo_libro", lib.getTitulo_libro());
        datos.put("c1", lib.getCod_editorial2());
        datos.put("c2", lib.getCod_categoria2());
        datos.put("c3", lib.getCod_autor2());
        
        JSONObject jsonResp = con.crearConexion(datos);
        
        if(jsonResp!=null){
            if(jsonResp.get("respuesta").toString().equalsIgnoreCase("1")){
                flag=1;
            }
        }
        
        return flag;
    }
}
