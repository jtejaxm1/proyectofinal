/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bds;

import com.mycompany.DTO.Autor;
import com.mycompany.DTO.Editorial;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author Compu Fire
 */
public class PerAutor {
    
    public int crearAutor(String nombre){
        int flag=0;
        Conexion con = new Conexion();
        JSONObject datos = new JSONObject();
        datos.put("tabla", "Autor");
        datos.put("codigo", "1");
        datos.put("Nombre_autor", nombre);
        
        JSONObject jsonResp = con.crearConexion(datos);
        
        if(jsonResp!=null){
            if(jsonResp.get("respuesta").toString().equalsIgnoreCase("1")){
                flag=1;
            }
        }
        
        return flag=1;
    }
    
    public int eliminarAutor(Autor aut){
        int flag=0;
        Conexion con = new Conexion();
        JSONObject datos = new JSONObject();
        datos.put("tabla", "Autor");
        datos.put("codigo", "3");
        datos.put("Cod_autor", aut.getCod_autor());
        datos.put("Nombre_autor", aut.getNombre_autor());
        
        JSONObject jsonResp = con.crearConexion(datos);
        
        if(jsonResp!=null){
            if(jsonResp.get("respuesta").toString().equalsIgnoreCase("1")){
                flag=1;
            }
        }
        
        return flag;
    }
    
    public int actualizarAutor(Autor aut){
        int flag=0;
        Conexion con = new Conexion();
        JSONObject datos = new JSONObject();
        datos.put("tabla", "Autor");
        datos.put("codigo", "2");
        datos.put("Cod_autor", aut.getCod_autor());
        datos.put("Nombre_autor", aut.getNombre_autor());
        
        JSONObject jsonResp = con.crearConexion(datos);
        
        if(jsonResp!=null){
            if(jsonResp.get("respuesta").toString().equalsIgnoreCase("1")){
                flag=1;
            }
        }
        
        return flag;
    }
    
    public List<Autor> findAllAutor(){
        Conexion con = new Conexion();
        List<Autor> lista = new ArrayList<Autor>();
        JSONObject datos = new JSONObject();
        datos.put("tabla", "Autor");
        datos.put("codigo", "4");
        
        JSONObject jsonResp = con.crearConexion(datos);
        
        JSONArray listaJson = new JSONArray();
        for(int i=0; i<jsonResp.size(); i++){
            Autor l1 = new Autor();
            JSONObject jsonResp1 = new JSONObject();
            String c= Integer.toString(i);
            jsonResp1 = (JSONObject) jsonResp.get(c);
            l1.setCod_autor(Integer.parseInt(jsonResp1.get("Cod_autor").toString()));
            l1.setNombre_autor((String) jsonResp1.get("Nombre_autor"));
            l1.setEstado(Integer.parseInt(jsonResp1.get("Estado").toString()));
            lista.add(l1);
        }
        
        return lista;
    }
}
