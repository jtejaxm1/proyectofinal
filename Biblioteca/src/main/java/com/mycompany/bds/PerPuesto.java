/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bds;

import com.mycompany.DTO.Editorial;
import com.mycompany.DTO.Puesto;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author Compu Fire
 */
public class PerPuesto {
    
    public int crearPuesto(String nombre){
        int flag=0;
        Conexion con = new Conexion();
        JSONObject datos = new JSONObject();
        datos.put("tabla", "Puesto");
        datos.put("codigo", "1");
        datos.put("Nombre_puesto", nombre);
        
        JSONObject jsonResp = con.crearConexion(datos);
        
        if(jsonResp!=null){
            if(jsonResp.get("respuesta").toString().equalsIgnoreCase("1")){
                flag=1;
            }
        }
        
        return flag=1;
    }
    
    public int eliminarPuesto(Puesto puest){
        int flag=0;
        Conexion con = new Conexion();
        JSONObject datos = new JSONObject();
        datos.put("tabla", "Puesto");
        datos.put("codigo", "3");
        datos.put("Cod_puesto", puest.getCod_puesto());
        datos.put("Nombre_puesto", puest.getNombre_puesto());
        
        JSONObject jsonResp = con.crearConexion(datos);
        
        if(jsonResp!=null){
            if(jsonResp.get("respuesta").toString().equalsIgnoreCase("1")){
                flag=1;
            }
        }
        
        return flag;
    }
    
    public int actualizarPuesto(Puesto puest){
        int flag=0;
        Conexion con = new Conexion();
        JSONObject datos = new JSONObject();
        datos.put("tabla", "Puesto");
        datos.put("codigo", "2");
        datos.put("Cod_puesto", puest.getCod_puesto());
        datos.put("Nombre_puesto", puest.getNombre_puesto());
        
        JSONObject jsonResp = con.crearConexion(datos);
        
        if(jsonResp!=null){
            if(jsonResp.get("respuesta").toString().equalsIgnoreCase("1")){
                flag=1;
            }
        }
        
        return flag;
    }
    
    public List<Puesto> findAllPuesto(){
        Conexion con = new Conexion();
        List<Puesto> lista = new ArrayList<Puesto>();
        JSONObject datos = new JSONObject();
        datos.put("tabla", "Puesto");
        datos.put("codigo", "4");
        
        JSONObject jsonResp = con.crearConexion(datos);
        JSONArray listaJson = new JSONArray();
        for(int i=0; i<jsonResp.size(); i++){
            Puesto l1 = new Puesto();
            JSONObject jsonResp1 = new JSONObject();
            String c= Integer.toString(i);
            jsonResp1 = (JSONObject) jsonResp.get(c);
            l1.setCod_puesto(Integer.parseInt(jsonResp1.get("Cod_puesto").toString()));
            l1.setNombre_puesto((String) jsonResp1.get("Nombre_puesto"));
            l1.setEstado(Integer.parseInt(jsonResp1.get("Estado").toString()));
            lista.add(l1);
        }
        
        return lista;
    }
}
