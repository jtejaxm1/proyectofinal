/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bds;

import com.mycompany.DTO.Editorial;
import com.mycompany.DTO.Libro;
import com.mycompany.bds.Conexion;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author Compu Fire
 */
public class PerEditorial {
    
    public int crearEditorial(String nombre){
        int flag=0;
        Conexion con = new Conexion();
        JSONObject datos = new JSONObject();
        datos.put("tabla", "Editorial");
        datos.put("codigo", "1");
        datos.put("Nombre_editorial", nombre);
        
        JSONObject jsonResp = con.crearConexion(datos);
        
        if(jsonResp!=null){
            if(jsonResp.get("respuesta").toString().equalsIgnoreCase("1")){
                flag=1;
            }
        }
        
        return flag;
    }
    
    public int eliminarEditorial(Editorial edi){
        int flag=0;
        Conexion con = new Conexion();
        JSONObject datos = new JSONObject();
        datos.put("tabla", "Editorial");
        datos.put("codigo", "3");
        datos.put("Cod_editorial", edi.getCod_editorial());
        datos.put("Nombre_editorial", edi.getNombre_editorial());
        
        JSONObject jsonResp = con.crearConexion(datos);
        
        if(jsonResp!=null){
            if(jsonResp.get("respuesta").toString().equalsIgnoreCase("1")){
                flag=1;
            }
        }
        
        return flag;
    }
    
    public int actualizarEditorial(Editorial edi){
        int flag=0;
        Conexion con = new Conexion();
        JSONObject datos = new JSONObject();
        datos.put("tabla", "Editorial");
        datos.put("codigo", "2");
        datos.put("Cod_editorial", edi.getCod_editorial());
        datos.put("Nombre_editorial", edi.getNombre_editorial());
        
        JSONObject jsonResp = con.crearConexion(datos);
        
        if(jsonResp!=null){
            if(jsonResp.get("respuesta").toString().equalsIgnoreCase("1")){
                flag=1;
            }
        }
        
        return flag;
    }
    
    public List<Editorial> findAllEditorial(){
        Conexion con = new Conexion();
        List<Editorial> lista = new ArrayList<Editorial>();
        JSONObject datos = new JSONObject();
        datos.put("tabla", "Editorial");
        datos.put("codigo", "4");
        
        JSONObject jsonResp = con.crearConexion(datos);
        JSONArray listaJson = new JSONArray();
        for(int i=0; i<jsonResp.size(); i++){
            Editorial l1 = new Editorial();
            JSONObject jsonResp1 = new JSONObject();
            String c= Integer.toString(i);
            jsonResp1 = (JSONObject) jsonResp.get(c);
            l1.setCod_editorial(Integer.parseInt(jsonResp1.get("Cod_editorial").toString()));
            l1.setNombre_editorial((String) jsonResp1.get("Nombre_editorial"));
            l1.setEstado(Integer.parseInt(jsonResp1.get("Estado").toString()));
            lista.add(l1);
        }
        
        return lista;
    }
    
}
