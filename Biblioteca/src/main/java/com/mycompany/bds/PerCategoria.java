/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bds;

import com.mycompany.DTO.Categoria;
import com.mycompany.DTO.Editorial;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author Compu Fire
 */
public class PerCategoria {
    
   public int crearCategoria(String nombre){
        int flag=0;
        Conexion con = new Conexion();
        JSONObject datos = new JSONObject();
        datos.put("tabla", "Categoria");
        datos.put("codigo", "1");
        datos.put("Nombre_categoria", nombre);
        
        JSONObject jsonResp = con.crearConexion(datos);
        
        if(jsonResp!=null){
            if(jsonResp.get("respuesta").toString().equalsIgnoreCase("1")){
                flag=1;
            }
        }
        
        return flag=1;
    }
    
    public int eliminarCategoria(Categoria cate){
        int flag=0;
        Conexion con = new Conexion();
        JSONObject datos = new JSONObject();
        datos.put("tabla", "Categoria");
        datos.put("codigo", "3");
        datos.put("Cod_categoria", cate.getCod_categoria());
        datos.put("Nombre_categoria", cate.getNombre_categoria());
        
        JSONObject jsonResp = con.crearConexion(datos);
        
        if(jsonResp!=null){
            if(jsonResp.get("respuesta").toString().equalsIgnoreCase("1")){
                flag=1;
            }
        }
        
        return flag;
    }
    
    public int actualizarCategoria(Categoria cate){
        int flag=0;
        Conexion con = new Conexion();
        JSONObject datos = new JSONObject();
        datos.put("tabla", "Categoria");
        datos.put("codigo", "2");
        datos.put("Cod_categoria", cate.getCod_categoria());
        datos.put("Nombre_categoria", cate.getNombre_categoria());
        
        JSONObject jsonResp = con.crearConexion(datos);
        
        if(jsonResp!=null){
            if(jsonResp.get("respuesta").toString().equalsIgnoreCase("1")){
                flag=1;
            }
        }
        
        return flag;
    }
    
    public List<Categoria> findAllCategoria(){
        Conexion con = new Conexion();
        List<Categoria> lista = new ArrayList<Categoria>();
        JSONObject datos = new JSONObject();
        datos.put("tabla", "Categoria");
        datos.put("codigo", "4");
        
        JSONObject jsonResp = con.crearConexion(datos);
        JSONArray listaJson = new JSONArray();
        for(int i=0; i<jsonResp.size(); i++){
            Categoria l1 = new Categoria();
            JSONObject jsonResp1 = new JSONObject();
            String c= Integer.toString(i);
            jsonResp1 = (JSONObject) jsonResp.get(c);
            l1.setCod_categoria(Integer.parseInt(jsonResp1.get("Cod_categoria").toString()));
            l1.setNombre_categoria((String) jsonResp1.get("Nombre_categoria"));
            l1.setEstado(Integer.parseInt(jsonResp1.get("Estado").toString()));
            lista.add(l1);
        }
        
        return lista;
    } 
}
