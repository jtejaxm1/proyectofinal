/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bds;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author Compu Fire
 */
public class Conexion {
    
    public JSONObject crearConexion(JSONObject datos){
        JSONObject jsonResp = new JSONObject();
        String uri="http://192.168.194.191:8083/WSBiblioteca/webresources/WSBiblioteca/BibliotecaWs?nombre="+Base64.getEncoder().encodeToString(datos.toJSONString().getBytes());
        System.out.println("el json de envio es: " + datos.toJSONString());
        System.out.println("la url de conexion es: " + uri);
        
        try {
            URL url = new URL(uri);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            System.out.println("conn: "+ conn);
            conn.setRequestMethod("GET");
            if(conn.getResponseCode()!=200){
                throw new RuntimeException("No se Pudo Conectar" + conn.getInputStream());
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String respuesta = br.readLine();
            byte[] data = Base64.getDecoder().decode(respuesta.getBytes());
            String dataJson = new String(data);
            JSONParser parser = new JSONParser();
            jsonResp = (JSONObject) parser.parse(dataJson);
            System.out.println("el json de respuesta es: "+jsonResp);
            
        } catch (Exception e) {
            System.out.println("Error al consumir el WEB SERVICE "+e);
        }
        
        return jsonResp;
    }
}
