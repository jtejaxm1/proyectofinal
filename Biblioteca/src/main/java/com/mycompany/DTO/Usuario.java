/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.DTO;

/**
 *
 * @author Compu Fire
 */
public class Usuario {

    /**
     * @return the Cod_usuario
     */
    public int getCod_usuario() {
        return Cod_usuario;
    }

    /**
     * @param Cod_usuario the Cod_usuario to set
     */
    public void setCod_usuario(int Cod_usuario) {
        this.Cod_usuario = Cod_usuario;
    }

    /**
     * @return the Nombre_puesto
     */
    public String getNombre_puesto() {
        return Nombre_puesto;
    }

    /**
     * @param Nombre_puesto the Nombre_puesto to set
     */
    public void setNombre_puesto(String Nombre_puesto) {
        this.Nombre_puesto = Nombre_puesto;
    }

    /**
     * @return the Cod_puesto
     */
    public int getCod_puesto() {
        return Cod_puesto;
    }

    /**
     * @param Cod_puesto the Cod_puesto to set
     */
    public void setCod_puesto(int Cod_puesto) {
        this.Cod_puesto = Cod_puesto;
    }

    /**
     * @return the Nombre_usuario
     */
    public String getNombre_usuario() {
        return Nombre_usuario;
    }

    /**
     * @param Nombre_usuario the Nombre_usuario to set
     */
    public void setNombre_usuario(String Nombre_usuario) {
        this.Nombre_usuario = Nombre_usuario;
    }

    /**
     * @return the Direccion_usuario
     */
    public String getDireccion_usuario() {
        return Direccion_usuario;
    }

    /**
     * @param Direccion_usuario the Direccion_usuario to set
     */
    public void setDireccion_usuario(String Direccion_usuario) {
        this.Direccion_usuario = Direccion_usuario;
    }

    /**
     * @return the Telefono
     */
    public int getTelefono() {
        return Telefono;
    }

    /**
     * @param Telefono the Telefono to set
     */
    public void setTelefono(int Telefono) {
        this.Telefono = Telefono;
    }

    /**
     * @return the Estado
     */
    public int getEstado() {
        return Estado;
    }

    /**
     * @param Estado the Estado to set
     */
    public void setEstado(int Estado) {
        this.Estado = Estado;
    }

    /**
     * @return the User_usuario
     */
    public String getUser_usuario() {
        return User_usuario;
    }

    /**
     * @param User_usuario the User_usuario to set
     */
    public void setUser_usuario(String User_usuario) {
        this.User_usuario = User_usuario;
    }

    /**
     * @return the Contrasena
     */
    public String getContrasena() {
        return Contrasena;
    }

    /**
     * @param Contrasena the Contrasena to set
     */
    public void setContrasena(String Contrasena) {
        this.Contrasena = Contrasena;
    }
    private int Cod_usuario;
    private String Nombre_puesto;
    private int Cod_puesto;
    private String Nombre_usuario;
    private String Direccion_usuario;
    private int Telefono;
    private int Estado;
    private String User_usuario;
    private String Contrasena;
}
