/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.DTO;

/**
 *
 * @author Compu Fire
 */
public class Autor {

    /**
     * @return the Cod_autor
     */
    public int getCod_autor() {
        return Cod_autor;
    }

    /**
     * @param Cod_autor the Cod_autor to set
     */
    public void setCod_autor(int Cod_autor) {
        this.Cod_autor = Cod_autor;
    }

    /**
     * @return the Nombre_autor
     */
    public String getNombre_autor() {
        return Nombre_autor;
    }

    /**
     * @param Nombre_autor the Nombre_autor to set
     */
    public void setNombre_autor(String Nombre_autor) {
        this.Nombre_autor = Nombre_autor;
    }

    /**
     * @return the Estado
     */
    public int getEstado() {
        return Estado;
    }

    /**
     * @param Estado the Estado to set
     */
    public void setEstado(int Estado) {
        this.Estado = Estado;
    }
 
    private int Cod_autor;
    private String Nombre_autor;
    private int Estado;
    
    //Esta es una prueba de versionamiento 06/11/2021
}
