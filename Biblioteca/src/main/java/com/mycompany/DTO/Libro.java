/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.DTO;

/**
 *
 * @author Compu Fire
 */
public class Libro {

    /**
     * @return the Cod_autor2
     */
    public String getCod_autor2() {
        return Cod_autor2;
    }

    /**
     * @param Cod_autor2 the Cod_autor2 to set
     */
    public void setCod_autor2(String Cod_autor2) {
        this.Cod_autor2 = Cod_autor2;
    }

    /**
     * @return the Cod_categoria2
     */
    public String getCod_categoria2() {
        return Cod_categoria2;
    }

    /**
     * @param Cod_categoria2 the Cod_categoria2 to set
     */
    public void setCod_categoria2(String Cod_categoria2) {
        this.Cod_categoria2 = Cod_categoria2;
    }

    /**
     * @return the cod_editorial2
     */
    public String getCod_editorial2() {
        return cod_editorial2;
    }

    /**
     * @param cod_editorial2 the cod_editorial2 to set
     */
    public void setCod_editorial2(String cod_editorial2) {
        this.cod_editorial2 = cod_editorial2;
    }

    /**
     * @return the Cod_editorial
     */
    public String getCod_editorial() {
        return Cod_editorial;
    }

    /**
     * @param Cod_editorial the Cod_editorial to set
     */
    public void setCod_editorial(String Cod_editorial) {
        this.Cod_editorial = Cod_editorial;
    }

    /**
     * @return the Cod_categoria
     */
    public String getCod_categoria() {
        return Cod_categoria;
    }

    /**
     * @param Cod_categoria the Cod_categoria to set
     */
    public void setCod_categoria(String Cod_categoria) {
        this.Cod_categoria = Cod_categoria;
    }

    /**
     * @return the Cod_autor
     */
    public String getCod_autor() {
        return Cod_autor;
    }

    /**
     * @param Cod_autor the Cod_autor to set
     */
    public void setCod_autor(String Cod_autor) {
        this.Cod_autor = Cod_autor;
    }

    /**
     * @return the Cod_libro
     */
    public int getCod_libro() {
        return Cod_libro;
    }

    /**
     * @param Cod_libro the Cod_libro to set
     */
    public void setCod_libro(int Cod_libro) {
        this.Cod_libro = Cod_libro;
    }

    /**
     * @return the Titulo_libro
     */
    public String getTitulo_libro() {
        return Titulo_libro;
    }

    /**
     * @param Titulo_libro the Titulo_libro to set
     */
    public void setTitulo_libro(String Titulo_libro) {
        this.Titulo_libro = Titulo_libro;
    }

    /**
     * @return the Existencia
     */
    public int getExistencia() {
        return Existencia;
    }

    /**
     * @param Existencia the Existencia to set
     */
    public void setExistencia(int Existencia) {
        this.Existencia = Existencia;
    }
    
    private int Cod_libro;
    private String Cod_editorial;
    private String cod_editorial2;
    private String Cod_categoria;
    private String Cod_categoria2;
    private String Cod_autor;
    private String Cod_autor2;
    private String Titulo_libro;
    private int Existencia;
    
}
