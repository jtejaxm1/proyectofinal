/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.DTO;

/**
 *
 * @author Compu Fire
 */
public class Prestamo {

    /**
     * @return the cod_prestamo
     */
    public int getCod_prestamo() {
        return cod_prestamo;
    }

    /**
     * @param cod_prestamo the cod_prestamo to set
     */
    public void setCod_prestamo(int cod_prestamo) {
        this.cod_prestamo = cod_prestamo;
    }

    /**
     * @return the cod_libro
     */
    public int getCod_libro() {
        return cod_libro;
    }

    /**
     * @param cod_libro the cod_libro to set
     */
    public void setCod_libro(int cod_libro) {
        this.cod_libro = cod_libro;
    }

    /**
     * @return the Nombre_libro
     */
    public String getNombre_libro() {
        return Nombre_libro;
    }

    /**
     * @param Nombre_libro the Nombre_libro to set
     */
    public void setNombre_libro(String Nombre_libro) {
        this.Nombre_libro = Nombre_libro;
    }

    /**
     * @return the cod_usuario
     */
    public int getCod_usuario() {
        return cod_usuario;
    }

    /**
     * @param cod_usuario the cod_usuario to set
     */
    public void setCod_usuario(int cod_usuario) {
        this.cod_usuario = cod_usuario;
    }

    /**
     * @return the Nombre_usuario
     */
    public String getNombre_usuario() {
        return Nombre_usuario;
    }

    /**
     * @param Nombre_usuario the Nombre_usuario to set
     */
    public void setNombre_usuario(String Nombre_usuario) {
        this.Nombre_usuario = Nombre_usuario;
    }

    /**
     * @return the fecha_prestamo
     */
    public String getFecha_prestamo() {
        return fecha_prestamo;
    }

    /**
     * @param fecha_prestamo the fecha_prestamo to set
     */
    public void setFecha_prestamo(String fecha_prestamo) {
        this.fecha_prestamo = fecha_prestamo;
    }

    /**
     * @return the fecha_devolucion
     */
    public String getFecha_devolucion() {
        return fecha_devolucion;
    }

    /**
     * @param fecha_devolucion the fecha_devolucion to set
     */
    public void setFecha_devolucion(String fecha_devolucion) {
        this.fecha_devolucion = fecha_devolucion;
    }

    /**
     * @return the estado
     */
    public int getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(int estado) {
        this.estado = estado;
    }
    private int cod_prestamo;
    private int cod_libro;
    private String Nombre_libro;
    private int cod_usuario;
    private String Nombre_usuario;
    private String fecha_prestamo;
    private String fecha_devolucion;
    private int estado;
}
