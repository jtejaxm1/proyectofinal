/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.DTO;

/**
 *
 * @author Compu Fire
 */
public class Categoria {

    /**
     * @return the Cod_categoria
     */
    public int getCod_categoria() {
        return Cod_categoria;
    }

    /**
     * @param Cod_categoria the Cod_categoria to set
     */
    public void setCod_categoria(int Cod_categoria) {
        this.Cod_categoria = Cod_categoria;
    }

    /**
     * @return the Nombre_categoria
     */
    public String getNombre_categoria() {
        return Nombre_categoria;
    }

    /**
     * @param Nombre_categoria the Nombre_categoria to set
     */
    public void setNombre_categoria(String Nombre_categoria) {
        this.Nombre_categoria = Nombre_categoria;
    }

    /**
     * @return the Estado
     */
    public int getEstado() {
        return Estado;
    }

    /**
     * @param Estado the Estado to set
     */
    public void setEstado(int Estado) {
        this.Estado = Estado;
    }
    private int Cod_categoria;
    private String Nombre_categoria;
    private int Estado;
}
