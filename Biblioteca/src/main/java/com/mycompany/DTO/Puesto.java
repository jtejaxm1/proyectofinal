/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.DTO;

/**
 *
 * @author Compu Fire
 */
public class Puesto {

    /**
     * @return the Cod_puesto
     */
    public int getCod_puesto() {
        return Cod_puesto;
    }

    /**
     * @param Cod_puesto the Cod_puesto to set
     */
    public void setCod_puesto(int Cod_puesto) {
        this.Cod_puesto = Cod_puesto;
    }

    /**
     * @return the Nombre_puesto
     */
    public String getNombre_puesto() {
        return Nombre_puesto;
    }

    /**
     * @param Nombre_puesto the Nombre_puesto to set
     */
    public void setNombre_puesto(String Nombre_puesto) {
        this.Nombre_puesto = Nombre_puesto;
    }

    /**
     * @return the Estado
     */
    public int getEstado() {
        return Estado;
    }

    /**
     * @param Estado the Estado to set
     */
    public void setEstado(int Estado) {
        this.Estado = Estado;
    }
    
    private int Cod_puesto;
    private String Nombre_puesto;
    private int Estado;
}
