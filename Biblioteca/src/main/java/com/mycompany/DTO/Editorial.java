/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.DTO;

/**
 *
 * @author Compu Fire
 */
public class Editorial {

    /**
     * @return the cod_editorial
     */
    public int getCod_editorial() {
        return cod_editorial;
    }

    /**
     * @param cod_editorial the cod_editorial to set
     */
    public void setCod_editorial(int cod_editorial) {
        this.cod_editorial = cod_editorial;
    }

    /**
     * @return the Nombre_editorial
     */
    public String getNombre_editorial() {
        return Nombre_editorial;
    }

    /**
     * @param Nombre_editorial the Nombre_editorial to set
     */
    public void setNombre_editorial(String Nombre_editorial) {
        this.Nombre_editorial = Nombre_editorial;
    }

    /**
     * @return the estado
     */
    public int getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(int estado) {
        this.estado = estado;
    }
    
    private int cod_editorial;
    private String Nombre_editorial;
    private int estado;
    
    //Hola esto es una prueba del versionamiento
    
}
