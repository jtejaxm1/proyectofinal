/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.biblioteca;

import com.mycompany.DTO.Libro;
import com.mycompany.DTO.Usuario;
import com.mycompany.bds.PerLibro;
import com.mycompany.bds.PerUsuario;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

/**
 *
 * @author Compu Fire
 */

@ManagedBean(name="bkn_Login")
public class LoginUI implements Serializable{

    /**
     * @return the id
     */
    public static int getId() {
        return id;
    }

    /**
     * @return the us
     */
    public String getUs() {
        return us;
    }

    /**
     * @param us the us to set
     */
    public void setUs(String us) {
        this.us = us;
    }

    /**
     * @return the contra
     */
    public String getContra() {
        return contra;
    }

    /**
     * @param contra the contra to set
     */
    public void setContra(String contra) {
        this.contra = contra;
    }

    /**
     * @return the usuario
     */
    public Usuario getUsuario() {
        if(usuario == null){
            usuario = new Usuario();
        }
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the pUsuario
     */
    public PerUsuario getpUsuario() {
        if(pUsuario == null){
            pUsuario = new PerUsuario();
        }
        return pUsuario;
    }

    /**
     * @param pUsuario the pUsuario to set
     */
    public void setpUsuario(PerUsuario pUsuario) {
        this.pUsuario = pUsuario;
    }

    /**
     * @return the images
     */
    public List<String> getImages() {
        return images;
    }

    /**
     * @return the opcion
     */
    public String getOpcion() {
        return opcion;
    }

    /**
     * @param opcion the opcion to set
     */
    public void setOpcion(String opcion) {
        this.opcion = opcion;
    }
    
    private String us;
    private String contra;
    private Usuario usuario;
    private PerUsuario pUsuario = new PerUsuario();
    private String opcion=null;
    private List<String> images;
    
    private static int id=0;
    
    public LoginUI(){
        
    }
    
    @PostConstruct
    public void init(){
        imagenes();
    }
    
    public void imagenes(){
        images = new ArrayList<String>();
        for (int i = 1; i <= 16; i++) {
            images.add("nature" + i + ".jpg");
        }
    }
    
    public String iniciarSesion(){
        String redireccion = null;
        int flag = pUsuario.iniciarsesion(usuario.getUser_usuario(), usuario.getContrasena(), opcion, usuario.getCod_usuario());
        if(flag==1){
            redireccion="VistaAdministrador";
        }else{
            if(flag==2){
                redireccion="VistaUsuario";
            }else{
                if(flag==10){
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "El ID Ingresado es Incorrecto"));
                }else{
                    if(flag==8){
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "El Usuario Ingresado es Incorrecto"));
                    }else{
                        if(flag==9){
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "La Contraseña Ingresada es Incorrecta"));
                        }
                    }
                }
            }
        }
        id=usuario.getCod_usuario();
        return redireccion;
    }
    
    public void limpiardatos(){
        this.usuario.setCod_usuario(0);
        this.usuario.setContrasena("");
        this.usuario.setUser_usuario("");
    }
    
    public void crearUsuario(){
        int flag=0;
        usuario.setCod_puesto(1);
        usuario.setUser_usuario(us);
        usuario.setContrasena(contra);
        System.out.println("el usuario es: "+usuario.getUser_usuario());
        boolean band = pUsuario.insertar(usuario);
        if(band){
            System.out.println("el programa va a buscar el id");
            usuario.setNombre_puesto("Usuario");
            flag=pUsuario.encontrarId(usuario);
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "Usuario Creado, Su ID es: "+flag));
    }
    
}
