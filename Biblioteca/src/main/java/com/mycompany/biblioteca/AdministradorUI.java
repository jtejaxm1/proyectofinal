/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.biblioteca;

import com.mycompany.DTO.Autor;
import com.mycompany.DTO.Categoria;
import com.mycompany.DTO.Editorial;
import com.mycompany.DTO.Libro;
import com.mycompany.DTO.Prestamo;
import com.mycompany.DTO.Puesto;
import com.mycompany.DTO.Usuario;
import com.mycompany.bds.PerAutor;
import com.mycompany.bds.PerCategoria;
import com.mycompany.bds.PerEditorial;
import com.mycompany.bds.PerLibro;
import com.mycompany.bds.PerPrestamo;
import com.mycompany.bds.PerPuesto;
import com.mycompany.bds.PerUsuario;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

/**
 *
 * @author Compu Fire
 */

@ManagedBean(name="bkn_Administrador")
public class AdministradorUI {

    /**
     * @return the prestamo
     */
    public Prestamo getPrestamo() {
        if(prestamo == null){
            prestamo = new Prestamo();
        }
        return prestamo;
    }

    /**
     * @param prestamo the prestamo to set
     */
    public void setPrestamo(Prestamo prestamo) {
        this.prestamo = prestamo;
    }

    /**
     * @return the pPrestamo
     */
    public PerPrestamo getpPrestamo() {
        if(pPrestamo == null){
            pPrestamo = new PerPrestamo();
        }
        return pPrestamo;
    }

    /**
     * @param pPrestamo the pPrestamo to set
     */
    public void setpPrestamo(PerPrestamo pPrestamo) {
        this.pPrestamo = pPrestamo;
    }

    /**
     * @return the listaPInactivos
     */
    public List<Prestamo> getListaPInactivos() {
        if(listaPInactivos == null){
            listaPInactivos = new ArrayList<Prestamo>();
        }
        return listaPInactivos;
    }

    /**
     * @param listaPInactivos the listaPInactivos to set
     */
    public void setListaPInactivos(List<Prestamo> listaPInactivos) {
        this.listaPInactivos = listaPInactivos;
    }

    /**
     * @return the listaPActivos
     */
    public List<Prestamo> getListaPActivos() {
        if(listaPActivos == null){
            listaPActivos = new ArrayList<Prestamo>();
        }
        return listaPActivos;
    }

    /**
     * @param listaPActivos the listaPActivos to set
     */
    public void setListaPActivos(List<Prestamo> listaPActivos) {
        this.listaPActivos = listaPActivos;
    }

    /**
     * @return the usuarios
     */
    public Usuario getUsuarios() {
        if(usuarios == null){
            usuarios= new Usuario();
        }
        return usuarios;
    }

    /**
     * @param usuarios the usuarios to set
     */
    public void setUsuarios(Usuario usuarios) {
        this.usuarios = usuarios;
    }

    /**
     * @return the listaUsuarios
     */
    public List<Usuario> getListaUsuarios() {
        if(listaUsuarios == null){
            listaUsuarios = new ArrayList<Usuario>();
        }
        return listaUsuarios;
    }

    /**
     * @param listaUsuarios the listaUsuarios to set
     */
    public void setListaUsuarios(List<Usuario> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    /**
     * @return the puesto
     */
    public Puesto getPuesto() {
        if(puesto==null){
            puesto = new Puesto();
        }
        return puesto;
    }

    /**
     * @param puesto the puesto to set
     */
    public void setPuesto(Puesto puesto) {
        this.puesto = puesto;
    }

    /**
     * @return the pPuesto
     */
    public PerPuesto getpPuesto() {
        if(pPuesto == null){
            pPuesto = new PerPuesto();
        }
        return pPuesto;
    }

    /**
     * @param pPuesto the pPuesto to set
     */
    public void setpPuesto(PerPuesto pPuesto) {
        this.pPuesto = pPuesto;
    }

    /**
     * @return the listaPuesto
     */
    public List<Puesto> getListaPuesto() {
        if(listaPuesto == null){
            listaPuesto = new ArrayList<Puesto>();
        }
        return listaPuesto;
    }

    /**
     * @param listaPuesto the listaPuesto to set
     */
    public void setListaPuesto(List<Puesto> listaPuesto) {
        this.listaPuesto = listaPuesto;
    }

    /**
     * @return the categoria
     */
    public Categoria getCategoria() {
        if(categoria == null){
            categoria = new Categoria();
        }
        return categoria;
    }

    /**
     * @param categoria the categoria to set
     */
    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    /**
     * @return the pCategoria
     */
    public PerCategoria getpCategoria() {
        if(pCategoria == null){
            pCategoria = new PerCategoria();
        }
        return pCategoria;
    }

    /**
     * @param pCategoria the pCategoria to set
     */
    public void setpCategoria(PerCategoria pCategoria) {
        this.pCategoria = pCategoria;
    }

    /**
     * @return the listaCategoria
     */
    public List<Categoria> getListaCategoria() {
        if(listaCategoria == null){
            listaCategoria = new ArrayList<Categoria>();
        }
        return listaCategoria;
    }

    /**
     * @param listaCategoria the listaCategoria to set
     */
    public void setListaCategoria(List<Categoria> listaCategoria) {
        this.listaCategoria = listaCategoria;
    }

    /**
     * @return the autor
     */
    public Autor getAutor() {
        if(autor==null){
            autor = new Autor();
        }
        return autor;
    }

    /**
     * @param autor the autor to set
     */
    public void setAutor(Autor autor) {
        this.autor = autor;
    }

    /**
     * @return the pAutor
     */
    public PerAutor getpAutor() {
        if(pAutor==null){
            pAutor = new PerAutor();
        }
        return pAutor;
    }

    /**
     * @param pAutor the pAutor to set
     */
    public void setpAutor(PerAutor pAutor) {
        this.pAutor = pAutor;
    }

    /**
     * @return the listaAutor
     */
    public List<Autor> getListaAutor() {
        if(listaAutor == null){
            listaAutor = new ArrayList<Autor>();
        }
        return listaAutor;
    }

    /**
     * @param listaAutor the listaAutor to set
     */
    public void setListaAutor(List<Autor> listaAutor) {
        this.listaAutor = listaAutor;
    }

    /**
     * @return the ediorial
     */
    public Editorial getEdiorial() {
        if(ediorial==null){
            ediorial = new Editorial();
        }
        return ediorial;
    }

    /**
     * @param ediorial the ediorial to set
     */
    public void setEdiorial(Editorial ediorial) {
        this.ediorial = ediorial;
    }

    /**
     * @return the pEditorial
     */
    public PerEditorial getpEditorial() {
        if(pEditorial==null){
            pEditorial = new PerEditorial();
        }
        return pEditorial;
    }

    /**
     * @param pEditorial the pEditorial to set
     */
    public void setpEditorial(PerEditorial pEditorial) {
        this.pEditorial = pEditorial;
    }

    /**
     * @return the listaEditorial
     */
    public List<Editorial> getListaEditorial() {
        if(listaEditorial == null){
            listaEditorial = new ArrayList<Editorial>();
        }
        return listaEditorial;
    }

    /**
     * @param listaEditorial the listaEditorial to set
     */
    public void setListaEditorial(List<Editorial> listaEditorial) {
        this.listaEditorial = listaEditorial;
    }

    /**
     * @return the pUsuario
     */
    public PerUsuario getpUsuario() {
        if(pUsuario==null){
            pUsuario = new PerUsuario();
        }
        return pUsuario;
    }

    /**
     * @param pUsuario the pUsuario to set
     */
    public void setpUsuario(PerUsuario pUsuario) {
        this.pUsuario = pUsuario;
    }

    /**
     * @return the listaUsuario
     */
    public List<Usuario> getListaUsuario() {
        if(listaUsuario==null){
            listaUsuario = new ArrayList<Usuario>();
        }
        return listaUsuario;
    }

    /**
     * @param listaUsuario the listaUsuario to set
     */
    public void setListaUsuario(List<Usuario> listaUsuario) {
        this.listaUsuario = listaUsuario;
    }

    /**
     * @return the usu
     */
    public Usuario getUsu() {
        return usu;
    }

    /**
     * @param usu the usu to set
     */
    public void setUsu(Usuario usu) {
        this.usu = usu;
    }

    /**
     * @return the listaLibro
     */
    public List<Libro> getListaLibro() {
        if(listaLibro == null){
            listaLibro = new ArrayList<Libro>();
        }
        return listaLibro;
    }

    /**
     * @param listaLibro the listaLibro to set
     */
    public void setListaLibro(List<Libro> listaLibro) {
        this.listaLibro = listaLibro;
    }

    /**
     * @return the libro
     */
    public Libro getLibro() {
        if(libro==null){
            libro = new Libro();
        }
        return libro;
    }

    /**
     * @param libro the libro to set
     */
    public void setLibro(Libro libro) {
        this.libro = libro;
    }

    /**
     * @return the pLibro
     */
    public PerLibro getpLibro() {
        if(pLibro == null){
            pLibro=new PerLibro();
        }
        return pLibro;
    }

    /**
     * @param pLibro the pLibro to set
     */
    public void setpLibro(PerLibro pLibro) {
        this.pLibro = pLibro;
    }

    private List<Libro> listaLibro;
    private Libro libro;
    private PerLibro pLibro = new PerLibro();
    int id1 = LoginUI.getId();
    private Usuario usu;
    private Usuario usuarios;
    private PerUsuario pUsuario = new PerUsuario();
    private List<Usuario> listaUsuario;
    private List<Usuario> listaUsuarios;
    private Editorial ediorial;
    private PerEditorial pEditorial = new PerEditorial();
    private List<Editorial> listaEditorial;
    private Autor autor;
    private PerAutor pAutor = new PerAutor();
    private List<Autor> listaAutor;
    private Categoria categoria;
    private PerCategoria pCategoria = new PerCategoria();
    private List<Categoria> listaCategoria;
    private Puesto puesto;
    private PerPuesto pPuesto = new PerPuesto();
    private List<Puesto> listaPuesto;
    private Prestamo prestamo;
    private PerPrestamo pPrestamo = new PerPrestamo();
    private List<Prestamo> listaPInactivos;
    private List<Prestamo> listaPActivos;
    
    public AdministradorUI(){
        
    }
    
    @PostConstruct
    public void init(){
        micuenta();
        llenarListas();
        llenarEditorial();
        llenarAutor();
        llenarCategoria();
        llenarPuesto();
        llenarUsuarios();
    }
    
    public void micuenta(){
        listaUsuario=pUsuario.cuenta(id1);
    }
    
    public void llenarPActivos(){
        listaPActivos=pPrestamo.findPrestamosActivos();
    }
    
    public void llenarPInactivos(){
        listaPInactivos=pPrestamo.findPrestamosInactivos();
    }
    
    public void llenarUsuarios(){
        listaUsuarios=pUsuario.findAllUsuario();
    }
    
    public void llenarPuesto(){
        listaPuesto=pPuesto.findAllPuesto();
    }
    
    public void llenarCategoria(){
        listaCategoria=pCategoria.findAllCategoria();
    }
    
    public void llenarListas(){
        System.out.println("El id es: "+id1);
        listaLibro=pLibro.findAllLibro();
    }
    
    public void llenarAutor(){
        listaAutor=pAutor.findAllAutor();
    }
    
    public void llenarEditorial(){
        listaEditorial=pEditorial.findAllEditorial();
    }
    
    public void crearLibro(){
        Libro lib = new Libro();
        lib.setCod_editorial2(this.libro.getCod_editorial2());
        lib.setCod_categoria2(this.libro.getCod_categoria2());
        lib.setCod_autor2(this.libro.getCod_autor2());
        lib.setTitulo_libro(this.libro.getTitulo_libro());
        lib.setExistencia(this.libro.getExistencia());
        
        int flag = pLibro.crearLibro(lib);
        if(flag==1){
            llenarListas();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "Libro Creado"));
        }
    }
    
    public void crearUsuario(){
        Usuario usu = new Usuario();
        usu.setCod_puesto(this.usuarios.getCod_puesto());
        usu.setNombre_usuario(this.usuarios.getNombre_usuario());
        usu.setDireccion_usuario(this.usuarios.getDireccion_usuario());
        usu.setTelefono(this.usuarios.getTelefono());
        usu.setEstado(1);
        usu.setUser_usuario(this.usuarios.getUser_usuario());
        usu.setContrasena(this.usuarios.getContrasena());
        
        boolean band =pUsuario.insertar(usu);
        if(band){
            llenarUsuarios();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "Usuario Creado"));
        }
        
    }
    
    public void crearPuesto(){
        String nom = puesto.getNombre_puesto();
        int flag=pPuesto.crearPuesto(nom);
        if(flag==1){
            llenarPuesto();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "Puesto Creado"));
        }
    }
    
    public void crearCategoria(){
        String nom = categoria.getNombre_categoria();
        int flag=pCategoria.crearCategoria(nom);
        if(flag==1){
            llenarCategoria();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "Categoria Creada"));
        }
    }
    
    public void crearEditorial(){
        String nom = ediorial.getNombre_editorial();
        int flag=pEditorial.crearEditorial(nom);
        if(flag==1){
            llenarEditorial();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "Editorial Creado"));
        }
    }
    
    public void crearAutor(){
        String nom = autor.getNombre_autor();
        int flag=pAutor.crearAutor(nom);
        if(flag==1){
            llenarAutor();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "Autor Creado"));
        }
    }
    
    public void eliminarPuesto(){
        int flag=this.pPuesto.eliminarPuesto(this.puesto);
        if(flag==1){
            llenarPuesto();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "Puesto Eliminado"));
        }
    }
    
    public void eliminarCategoria(){
        int flag=this.pCategoria.eliminarCategoria(this.categoria);
        if(flag==1){
            llenarCategoria();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "Categoria Eliminada"));
        }
    }
    
    public void eliminarEditorial(){
        int flag=this.pEditorial.eliminarEditorial(this.ediorial);
        if(flag==1){
            llenarEditorial();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "Editorial Eliminado"));
        }
    }
    
    public void eliminarAutor(){
        int flag=this.pAutor.eliminarAutor(this.autor);
        if(flag==1){
            llenarAutor();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "Autor Eliminado"));
        }
    }
    
    public void cancelarPrestamosActivos(){
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "Prestamo No Modificado"));
    }
    
    public void cancelarUsuario(){
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "Usuario No Modificado"));
    }
    
    public void cancelarPuesto(){
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "Puesto No Modificado"));
    }
    
    public void cancelarCategoria(){
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "Categoria No Modificada"));
    }
    
    public void cancelarEditorial(){
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "Editorial No Modificada"));
    }
    
    public void cancelarAutor(){
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "Autor No Modificado"));
    }
    
    public void cancelarCuenta(){
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "Cuenta No Modificada"));
    }
    
    public void cancelarLibro(){
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "Libro No Modificado"));
    }
    
    public void actualizarPrestamo(int codigo, int c1, int c2, String f1, String f2, int estado){
        Prestamo pres = new Prestamo();
        int flag=0;
        pres.setCod_prestamo(codigo);
        pres.setCod_libro(c1);
        pres.setCod_usuario(c2);
        pres.setFecha_prestamo(f1);
        pres.setFecha_devolucion(f2);
        pres.setEstado(estado);
        flag=pPrestamo.actualizarPrestamo(pres);
        if(flag==1){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "Prestamo Modificado"));
            llenarPActivos();
        }
        
    }
    
    public void actualizarPuesto(int codigo, String nombre, int estado){
        Puesto puest = new Puesto();
        int flag=0;
        puest.setCod_puesto(codigo);
        puest.setNombre_puesto(nombre);
        puest.setEstado(estado);
        flag=pPuesto.actualizarPuesto(puest);
        if(flag==1){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "Puesto Modificado"));
            llenarPuesto();
        }
    }
    
    public void actualizarCategoria(int codigo, String nombre, int estado){
        Categoria cate = new Categoria();
        int flag=0;
        cate.setCod_categoria(codigo);
        cate.setNombre_categoria(nombre);
        cate.setEstado(estado);
        flag=pCategoria.actualizarCategoria(cate);
        if(flag==1){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "Categoria Modificada"));
            llenarCategoria();
        }
    }
    
    public void actualizarAutor(int codigo, String nombre, int estado){
        Autor aut = new Autor();
        int flag=0;
        aut.setCod_autor(codigo);
        aut.setNombre_autor(nombre);
        aut.setEstado(estado);
        flag=pAutor.actualizarAutor(aut);
        if(flag==1){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "Autor Modificado"));
            llenarAutor();
        }
    }
    
    public void actualizarEditorial(int codigo, String nombre, int estado){
        Editorial edi = new Editorial();
        int flag=0;
        edi.setCod_editorial(codigo);
        edi.setNombre_editorial(nombre);
        edi.setEstado(estado);
        flag=pEditorial.actualizarEditorial(edi);
        if(flag==1){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "Editorial Modificado"));
            llenarEditorial();
        }
    }
    
    public void actualizarUsuario(int cod_usu, int cod_pue, String nombre, String direccion, int tele, int estado, String user, String contra){
        Usuario usu = new Usuario();
        int flag=0;
        usu.setCod_usuario(cod_usu);
        usu.setCod_puesto(cod_pue);
        usu.setNombre_usuario(nombre);
        usu.setDireccion_usuario(direccion);
        usu.setTelefono(tele);
        usu.setEstado(estado);
        usu.setUser_usuario(user);
        usu.setContrasena(contra);
        flag=pUsuario.actualizarUsuario(usu);
        if(flag==1){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "Cuenta Modificada"));
            llenarUsuarios();
        }
    }
    
    public void actualizarCuenta(String nombre, String direccion, int telefono,int estado, String user, String contrasena){
        Usuario usu = new Usuario();
        int flag=0;
        usu.setCod_usuario(listaUsuario.get(0).getCod_usuario());
        usu.setCod_puesto(2);
        usu.setNombre_usuario(nombre);
        usu.setDireccion_usuario(direccion);
        usu.setTelefono(telefono);
        usu.setEstado(estado);
        usu.setUser_usuario(user);
        usu.setContrasena(contrasena);
        flag=pUsuario.actualizarCuentaEspecial(usu);
        if(flag==1){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "Cuenta Modificada"));
            micuenta();
        }
    }
    
    public void actualizarLibro(int cod_libro, String cod_editorial, String cod_categoria, String cod_autor, String titulo, int existencia){
        int flag=0;
        Libro lib = new Libro();
        lib.setCod_libro(cod_libro);
        lib.setCod_editorial2(cod_editorial);
        lib.setCod_categoria2(cod_categoria);
        lib.setCod_autor2(cod_autor);
        lib.setTitulo_libro(titulo);
        lib.setExistencia(existencia);
        flag=pLibro.actualizarLibro(lib);
        if(flag==1){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "Libro Modificado"));
            llenarListas();
        }
    }
    
}
