/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.biblioteca;

import com.mycompany.DTO.Libro;
import com.mycompany.DTO.Prestamo;
import com.mycompany.DTO.Usuario;
import com.mycompany.bds.PerLibro;
import com.mycompany.bds.PerPrestamo;
import com.mycompany.bds.PerUsuario;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

/**
 *
 * @author Compu Fire
 */

@ManagedBean(name="bkn_Usuario")
public class UsuarioUI {

    /**
     * @return the prestamo
     */
    public Prestamo getPrestamo() {
        if(prestamo == null){
            prestamo = new Prestamo();
        }
        return prestamo;
    }

    /**
     * @param prestamo the prestamo to set
     */
    public void setPrestamo(Prestamo prestamo) {
        this.prestamo = prestamo;
    }

    /**
     * @return the pPrestamo
     */
    public PerPrestamo getpPrestamo() {
        if(pPrestamo == null){
            pPrestamo = new PerPrestamo();
        }
        return pPrestamo;
    }

    /**
     * @param pPrestamo the pPrestamo to set
     */
    public void setpPrestamo(PerPrestamo pPrestamo) {
        this.pPrestamo = pPrestamo;
    }

    /**
     * @return the llistaPrestamo
     */
    public List<Prestamo> getLlistaPrestamo() {
        if(llistaPrestamo==null){
            llistaPrestamo = new ArrayList<Prestamo>();
        }
        return llistaPrestamo;
    }

    /**
     * @param llistaPrestamo the llistaPrestamo to set
     */
    public void setLlistaPrestamo(List<Prestamo> llistaPrestamo) {
        this.llistaPrestamo = llistaPrestamo;
    }

    /**
     * @return the usu
     */
    public Usuario getUsu() {
        if(usu == null){
            usu = new Usuario();
        }
        return usu;
    }

    /**
     * @param usu the usu to set
     */
    public void setUsu(Usuario usu) {
        this.usu = usu;
    }

    /**
     * @return the pUsuario
     */
    public PerUsuario getpUsuario() {
        if(pUsuario == null){
            pUsuario = new PerUsuario();
        }
        return pUsuario;
    }

    /**
     * @param pUsuario the pUsuario to set
     */
    public void setpUsuario(PerUsuario pUsuario) {
        this.pUsuario = pUsuario;
    }

    /**
     * @return the listaUsuario
     */
    public List<Usuario> getListaUsuario() {
        if(listaUsuario==null){
            listaUsuario = new ArrayList<Usuario>();
        }
        return listaUsuario;
    }

    /**
     * @param listaUsuario the listaUsuario to set
     */
    public void setListaUsuario(List<Usuario> listaUsuario) {
        this.listaUsuario = listaUsuario;
    }

    /**
     * @return the images
     */
    public List<String> getImages() {
        return images;
    }

    /**
     * @return the libro
     */
    public Libro getLibro() {
        if(libro == null){
            libro = new Libro();
        }
        return libro;
    }

    /**
     * @param libro the libro to set
     */
    public void setLibro(Libro libro) {
        this.libro = libro;
    }

    /**
     * @return the pLibro
     */
    public PerLibro getpLibro() {
        if(pLibro == null){
            pLibro = new PerLibro();
        }
        return pLibro;
    }

    /**
     * @param pLibro the pLibro to set
     */
    public void setpLibro(PerLibro pLibro) {
        this.pLibro = pLibro;
    }

    /**
     * @return the listaLibro
     */
    public List<Libro> getListaLibro() {
        if(listaLibro == null){
            listaLibro = new ArrayList<Libro>();
        }
        return listaLibro;
    }

    /**
     * @param listaLibro the listaLibro to set
     */
    public void setListaLibro(List<Libro> listaLibro) {
        this.listaLibro = listaLibro;
    }
    
    int id1 = LoginUI.getId();
    private List<String> images;
    private Libro libro;
    private PerLibro pLibro = new PerLibro();
    private List<Libro> listaLibro;
    private Usuario usu;
    private PerUsuario pUsuario = new PerUsuario();
    private List<Usuario> listaUsuario;
    private Prestamo prestamo;
    private PerPrestamo pPrestamo = new PerPrestamo();
    private List<Prestamo> llistaPrestamo;
    
    
    public UsuarioUI(){
        
    }
    
    @PostConstruct
    public void init(){
        imagenes();
        micuenta();
        llenarLibros();
    }
    
    public void imagenes(){
        images = new ArrayList<String>();
        for (int i = 1; i <= 5; i++) {
            images.add("fondo" + i + ".jpg");
        }
    }
    
    public void llenarLibros(){
        listaLibro=pLibro.findAllLibroUsuario();
    }
    
    public void micuenta(){
        listaUsuario=pUsuario.cuenta(id1);
    }
    
    public void cancelarCuenta(){
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "Cuenta No Modificada"));
    }
    
    public void actualizarCuenta(int cod, String nombre, String direccion, int telefono,int estado, String user, String contrasena){
        Usuario usu = new Usuario();
        int flag=0;
        usu.setCod_usuario(listaUsuario.get(0).getCod_usuario());
        usu.setCod_puesto(cod);
        usu.setNombre_usuario(nombre);
        usu.setDireccion_usuario(direccion);
        usu.setTelefono(telefono);
        usu.setEstado(estado);
        usu.setUser_usuario(user);
        usu.setContrasena(contrasena);
        flag=pUsuario.actualizarCuentaEspecial(usu);
        if(flag==1){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "Cuenta Modificada"));
            micuenta();
        }
    }
    
    public void crearPrestamo(){
        int flag=0;
        Prestamo pres = new Prestamo();
        pres.setCod_libro(this.prestamo.getCod_libro());
        pres.setCod_usuario(id1);
        pres.setEstado(1);
        pres.setFecha_prestamo(this.prestamo.getFecha_prestamo());
        pres.setFecha_devolucion(this.prestamo.getFecha_devolucion());
        flag=pPrestamo.crearPrestamo(pres, id1);
        if(flag==1){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage (FacesMessage.SEVERITY_INFO, "Aviso", "Prestamo Creado"));
        }
    }
}
